import unittest
import Lesson5


class TestTriangle (unittest.TestCase):

    def test_not_trianlge(self):
        triangle_kind = Lesson5.trojkat(2,5,100)
        self.assertEqual ("This isn't a triangle", triangle_kind)

    def test_simple(self):
        triangle_kind = Lesson5.trojkat(7,7,7)
        self.assertEqual ("This is simply triangle", triangle_kind)


    def test_is_pythagorean(self):
        triangle_kind = Lesson5.trojkat(9,40,41)
        self.assertEqual ("This is a pythagorean triangle", triangle_kind)

    def test_is_egyptian(self):
        triangle_kind = Lesson5.trojkat(9,12,15)
        self.assertEqual ("This is a egyptian triangle", triangle_kind)


if __name__ == '__main__':
    unittest.main ()
