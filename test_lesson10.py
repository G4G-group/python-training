import unittest
import Lesson10


class SilniaCalc (unittest.TestCase):

    def test_one(self):
        silnia_try = Lesson10.silnia(1, 4)
        self.assertEqual (24, silnia_try)

    def test_two(self):
        silnia_try = Lesson10.silnia (1, 8)
        self.assertEqual (40320, silnia_try)


if __name__ == '__main__':
    unittest.main ()
