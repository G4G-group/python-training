import unittest
import Lesson31


class bmi_calculator(unittest.TestCase):
    def test_under(self):
        bmi_under = Lesson31.BMI_calc(1.80, 56)
        self.assertEqual ("Underweight", bmi_under)

    def test_normal(self):
        bmi_norm = Lesson31.BMI_calc(1.66, 55)
        self.assertEqual ("Normal", bmi_norm)

    def test_little(self):
        bmi_little = Lesson31.BMI_calc(1.66, 69)
        self.assertEqual ("Little overweight", bmi_little)

    def test_overweight(self):
        bmi_over = Lesson31.BMI_calc(1.66, 75)
        self.assertEqual ("Overweight", bmi_over)

    def test_obesity(self):
        bmi_obes = Lesson31.BMI_calc(1.66, 90)
        self.assertEqual ("Obesity I", bmi_obes)

    def test_obesityII(self):
        bmi_obes_2 = Lesson31.BMI_calc(1.66, 100)
        self.assertEqual ("Obesity II", bmi_obes_2)

    def test_obesityIII(self):
        bmi_obes_3 = Lesson31.BMI_calc(1.66, 120)
        self.assertEqual ("Obesity III", bmi_obes_3)

if __name__ == '__main__':
    unittest.main ()
