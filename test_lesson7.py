import unittest
import Lesson7


class Div(unittest.TestCase):
    def test_one(self):
        division_one = Lesson7.division(12)
        self.assertEqual ("Hurra!", division_one)

    def test_two(self):
        division_two = Lesson7.division(9)
        self.assertEqual ("Dzieli się przez 3", division_two)

    def test_three(self):
        division_three = Lesson7.division(8)
        self.assertEqual ("Dzieli się przez 4", division_three)

    def test_four(self):
        division_four = Lesson7.division(10)
        self.assertEqual ("*", division_four)

if __name__ == '__main__':
    unittest.main ()
