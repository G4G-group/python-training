x = 1
y = 7

if (x < y):
    print("yes")

if x > y:
    print("no")
if y < x:
    print("yes")

a = 5
b = 3

if (a > b):
     print("Welcome")
else:
     print("The condition was not true")

age = 23
if (age < 13):
     print("You are young")
elif (age >= 13 and age < 18):
    print("You are a teenager")
else:
     print("You are an adult")

#For loops

list1 = ["Blue", "Green", "Orange", "yellow"]
tup1 = {23, 16, 99, 64}
for item in list1:
    print(item)
for item in tup1:
    print(tup1)

for i in range(0,11):
    print(i)

for j in range(1,10,2):
    print(j)

for s in range(0,51,5):
    print(s)

#While loops

c = 0
while c < 5:
    print(c)
    c = c + 1

d = 1
while d < 6:
    print(d)
    if d == 4:
        break
    d = d + 1

e = 0
while e < 10:
    e = e + 1
    if e == 5:
        continue
    print(e)

f = 2
while f < 8:
    f = f + 1
    if f == 5:
        pass
    print(f)
#Functions

def HelloWorld():
    print("Hello World")

HelloWorld()

def Greeting(name):
    print("Welcome " + name + "!")
Greeting("Ewa")
Greeting("Tomasz")

def Add(num1, num2):
    print(num1 + num2)
Add(20,34)
Add(0,8921)

def returnAdd(num1, num2, num3):
    return (num1 + num2 + num3)
sum = returnAdd(19,23,4)
print(sum)

def my_form(fname, lname):
    print(fname + " " + lname)
my_form("Joanna", "Toniasz")
my_form("Roksana", "Wungiel")

def my_function(*dogs):
    print("The biggest dog is " + dogs[2])
my_function("Max", "Rofus","Linus", "Cezar")

def my_func(cat1, cat2, cat3, cat4):
    print("The most expensive cat is " + cat2)
my_func(cat1 = "Blue", cat2 = "Roger", cat3 = "Diamond", cat4 = "meow")

def my_funct(**book):
    print("The best book in 2019 is " + book["title"])
my_funct(title = "Księgi Jakubowe")

#Built-in functions

p = abs(79)
print(p)

r = abs(-39.89)
print(r)

s = bool(0)
print(s)

t = bool(111)
print(t)

class Cat:
    name = "Shine"
    age = 5
print(dir(Cat))

problem = ("Hi")
print(help(problem.upper))
print(help(problem.splitlines))

u = "print(4903)"
eval(u)

w = "print('Welcome to the show!')"
eval(w)

x = 'name = "John"\nprint(name)'
exec(x)

y = 3
print(str(y))
print(float(y))
print(int(y))

class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def printName(self):
        print("Your name is " + self.name)
    def printAge(self):
        print("Your age is " + self.age)
p1 = Person("Michael", "23")
p1.printName()
p1.printAge()

#Inheritance

class Person:
    def __init__(self, fname, lname):
        self.firstname = fname
        self.lastname = lname

    def printname(self):
        print(self.firstname, self.lastname)
x = Person("Aleksandra", "Mroz")
x.printname()

class Student(Person):
    def __init__(self, fname, lname):
        super().__init__(fname, lname)
        self.graduationyear = 2019

    def welcome(self):
        print("Wlecome ", self.firstname, self.lastname, "to the class of ", self.graduationyear)

x = Student("Anna", "Horoszko")
print(x.graduationyear)
x.welcome()





