import unittest
import Lesson42


class exercises_with_string (unittest.TestCase):

    def test_lenght_of_string(self):
        test_lengh = Lesson42.len_func("Kurs pythona jest prosty")
        self.assertEqual (24 , test_lengh)

    def test_with_letter_replace(self):
        test_replace = Lesson42.replace_func("Kurs pythona jest prosty")
        self.assertEqual ("Kórs pythona jest prosty", test_replace)

    def test_patr_of_string(self):
        test_part = Lesson42.part_of_str("Kurs pythona jest prosty")
        self.assertEqual ("na je", test_part)



if __name__ == '__main__':
    unittest.main ()
