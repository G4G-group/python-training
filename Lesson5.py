def trojkat(a, b, c):
    if (a / 3 == b / 4 == c / 5):
        return "This is a egyptian triangle"
    elif (a ** 2 + b ** 2 == c ** 2):
        return "This is a pythagorean triangle"
    elif a < b + c and b < a + c and c < a + b:
        return "This is simply triangle"
    else:
        return "This isn't a triangle"


if __name__ == "__main__":

    w = trojkat (9, 12, 15)
    print (w)
    assert w == "This is a egyptian triangle"

    x = trojkat (9, 40, 41)
    print (x)
    assert x == "This is a pythagorean triangle"

    y = trojkat (7, 7, 7)
    print (y)
    assert y == "This is simply triangle"

    z = trojkat (31, 2, 54)
    print (z)
    assert z == "This isn't a triangle"
