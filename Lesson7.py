
def division(d):
    if d % 3 == 0 and d % 4 == 0:
        return "Hurra!"
    elif d % 3 == 0:
        return "Dzieli się przez 3"
    elif d % 4 == 0:
        return "Dzieli się przez 4"
    else:
        return "*"


if __name__ == "__main__":
    dig_1 = division(12)
    print (dig_1)
    assert dig_1 == "Hurra!"

    dig_2 = division(9)
    print (dig_2)
    assert dig_2 == "Dzieli się przez 3"

    dig_3 = division(8)
    print (dig_3)
    assert dig_3 == "Dzieli się przez 4"

    dig_4 = division(10)
    print (dig_4)
    assert dig_4 == "*"