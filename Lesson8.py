
'''''

szer = 42
print("-" * szer)
print("|  C |     /    |    X    |  <-  |")
print("*" * szer)
print("| %6.3f | %16s | %10s |" % (9.58, "Usain Bolt", "16.08.2009"))
print("| %6.3f | %16s | %10s |" % (9.69, "Tyson Gay", "20.09.2009"))
print("| %6.3f | %16s | %10s |" % (9.69, "Yohan Blake", "23.09.2012"))
print("| %6.3f | %16s | %10s |" % (9.74, "Asafa Powell", "2.09.2008"))
print("-" * szer)

'''''
szer = 42
a = 1
print ("Mnożenie przez 1:")
print ("*" * szer)
for i in range(0, 10):
    print("{:5d}".format(a * (i + 1)), "|", end=" ")

b = 2
print ("\nMnożenie przez 2:")
print ("*" * szer)
for i in range(0, 10):
    print ("{:5d}".format (b * (i + 1)), "|", end=" ")
c = 3
print ("\nMnożenie przez 3:")
print ("*" * szer)
for i in range(0, 10):
    print ("{:5d}".format (c * (i + 1)), "|", end=" ")
d = 4
print ("\nMnożenie przez 4:")
print ("*" * szer)
for i in range(0, 10):
    print ("{:5d}".format (d * (i + 1)), "|", end=" ")
e = 5
print ("\nMnożenie przez 5:")
print ("*" * szer)
for i in range(0, 10):
    print ("{:5d}".format (e * (i + 1)), "|", end=" ")
f = 6
print ("\nMnożenie przez 6:")
print ("*" * szer)
for i in range(0, 10):
    print ("{:5d}".format (f * (i + 1)), "|", end=" ")
g = 7
print ("\nMnożenie przez 7:")
print ("*" * szer)
for i in range(0, 10):
    print ("{:5d}".format (g * (i + 1)), "|", end=" ")
h = 8
print ("\nMnożenie przez 8:")
print ("*" * szer)
for i in range(0, 10):
    print ("{:5d}".format (h * (i + 1)), "|", end=" ")
j = 9
print ("\nMnożenie przez 9:")
print ("*" * szer)
for i in range(0, 10):
    print ("{:5d}".format (j * (i + 1)), "|", end=" ")
k = 10
print ("\nMnożenie przez 10:")
print ("*" * szer)
for i in range(0, 10):
    print ("{:5d}".format (k * (i + 1)), "|", end=" ")
'''''
    print("| %2s | %2s | %2s | %2s | %2s | %2s | %2s | %2s | %2s | %2s |" % (a * (i +1))
'''''
