#String formatting

print("1 in polish: {} \n2 in polish: {}".format('jeden', 'dwa') )

print("Number {} is before {}".format('7','8'))

print("The World Records for longest time an abdomianl plank position is {} and was achived by {}".format('8.0100', 'Mao Weidong'))

print("The official world records in {} meters are held by {} with {:.2f}".format('10000', 'Kenenisa Bekele', 26.1753))

print("Grocery list: {2}, {1}, {0}".format('bananas', 'milk', 'butter'))

#Konwersja metr na cal
print("Wpisz odległość w metrach")
metr = float(input())
cal = 39.370078740157 * metr
print("{} metr to {:.2f} cal".format(metr, cal))

#Obliczanie stanu konta za kilka lat
print("Jaki jest stan początkowy konta?")
poczatek = float(input())
print("Jaka jest stopa oprocentowania?")
procent = float(input())
print("Na ile lat zakładana lokata?")
lata = float(input())
end = poczatek*(1 + procent*lata)
print("Po {} latach kapitał będzie wynosił {}".format(lata, end))