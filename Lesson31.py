def BMI_calc(hight, weight):

    BMI = weight / (hight ** 2)

    if (BMI < 18.5):
        return "Underweight"
    elif (18.5 <= BMI < 24):
        return "Normal"
    elif (24 <= BMI < 26.5):
        return "Little overweight"
    elif (26.5 <= BMI < 30):
        return "Overweight"
    elif (30 <= BMI < 35):
        return "Obesity I"
    elif (35 <= BMI < 40):
        return "Obesity II"
    else:
        return "Obesity III"

BMI_calc(1.66 , 75)
